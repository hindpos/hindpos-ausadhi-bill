const fs = require('fs');
const Handlebars = require('handlebars');
const path = require('path');

const source = fs.readFileSync(path.join(process.env.HOME, '.invoice', 'template', 'index.handlebars'), 'utf8');
const invoiceDir = path.join(process.env.HOME, '.invoice');
if (!fs.existsSync(invoiceDir)) {
  fs.mkdirSync(invoiceDir);
}

const dataToHTML = async (data) => {
  const template = Handlebars.compile(source);
  const result = template(data);
  fs.writeFileSync(path.join(invoiceDir, `invoice.html`), result);
};

module.exports = {
  dataToHTML: dataToHTML
};
